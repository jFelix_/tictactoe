class TicTacToe{

	constructor(){
		this.win = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7],
		[2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]];
		this.board = [[],[],[]];
	}
	playX(row, column){
		if (!validatePosition(board[row][column])  && row < 3 && column < 3) {
			this.board[row][column] = "X";
		}
	}
	playO(row, column){
		if (!validatePosition(board[row][column])  && row < 3 && column < 3) {
			this.board[row][column] = "O";
		}
	}
	winner() {
		for (var i = 0; i < board.length; i++) {
			var CountXH = 0, CountOH = 0, CountXV = 0, CountOV = 0;
			for (var j = 0; j < board[i].length; j++) {
				if (board[i][j] === "X") {
					CountXH++;
				}else if (board[i][j] === "O") {
					CountOH++;
				}
				if (board[j][i] === "X") {
					CountXV++;
				}else if (board[j][i] === "O") {
					CountOV++;
				}
			}
			if (CountXH === 3 || CountXV === 3) {
				return 'X';
			}else if (CountOH === 3 || CountOV === 3) {
				return 'Y';
			}else{
				if(board[0][0] === "X" && board[1][1] === "X" && board[2][2] === "X" || 
					board[2][0] === "X" && board[1][1] === "X" && board[0][2] === "X")
					return 'X';
				else if(board[0][0]  === "O" && board[1][1] === "O" && board[2][2] === "O" || 
					board[2][0] === "O" && board[1][1] === "O" && board[0][2] === "O")
					return 'Y';
			}
		}
	}
	validatePosition(position) {
		if (isString(position) === true) {
			return true;
		}
		return false;
	}
	isString(myVar){
		if (typeof myVar === 'string' || myVar instanceof String)
			return true;
		else
			return false;
	}
}